// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  Image,
  List,
  ListItem,
  Notes,
  Quote,
  Slide,
  Text
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

const images = {
  formidagon: require('../assets/star.jpg'),
  goodWork: require('../assets/no.png'),
  dranger: require('../assets/dranger.jpg')
};

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: '#55505c',
    secondary: '#faf33e',
    tertiary: '#7fc6a4',
    quaternary: '#d6f8d6'
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica'
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide transition={['zoom']} bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
          Saying NO - decir no
          </Heading>
          <Text margin="15px 0 0" textColor="tertiary" fit bold>
          Kevin Alejandro Vanegas
          </Text>
          <Text size={6} textColor="quaternary">
          disponible en https://gitlab.com/kigama/expo-decir-no
          </Text>
          
        </Slide>
        <Slide transition={['fade']} bgColor="primary" textColor="secondary">
          <BlockQuote>
            <Quote size={6} textColor="quaternary">"Hazlo o no lo hagas. Pero no lo intentes."</Quote>
            <Cite margin="10px 0 0 30px">Yoda</Cite>
            <Image src={images.formidagon} width={300} />
          </BlockQuote>          
        </Slide>
        <Slide>
          <Image src={images.goodWork} width={500} />
          <Notes>gifs work too</Notes>
        </Slide>
        <Slide transition={['fade']} bgColor="primary" textColor="tertiary">
          <Heading size={6} textColor="secondary" caps>
            Significado
          </Heading>
          <List>
            <ListItem margin="15px 0 0" textAlign="justify">Es profesional decir no cuando un empleador / jefe intentan forzar una fecha imposible de cumplir o funcionalidades imposibles de implementar en la fecha límite.</ListItem>
            <ListItem margin="15px 0 0" textAlign="justify">Los profesionales dicen la verdad al poder. Los profesionales tienen el coraje de decir no a sus gerentes.</ListItem>

          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary" textColor="primary">
          <Heading size={6} textColor="secondary" caps>
            Consecuencias
          </Heading>
          <List>
            <ListItem margin="15px 0 0" textAlign="justify">Cuando no se informa claramente y/o no se mantiene esta posición de que es imposible cumplir lo deseado por el jefe /empleador, suelen haber catastróficas consecuencias</ListItem>
            <Image src={images.dranger} width={800} />
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary" textColor="primary">
          <Heading size={1} textColor="secondary">
          Decir “No” profesionalmente
          </Heading>
          
          <Heading size={4} textColor="quaternary">
          -Saber decir que no, es básico para ser productivo-
          </Heading>
          <List>
            <ListItem>Dar estimados claros y precisos</ListItem>
            <ListItem>No decir “Lo intentare” en lugar de “No”</ListItem>
            <ListItem>Dejar claro que es posible y que no lo es</ListItem>
            <ListItem>No permitir que se hagan falsas promesas a jefes/clientes</ListItem>            
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="primary" textColor="tertiary">
          <Heading size={6} textColor="quaternary" caps>
          Intentando...
          </Heading>
          <List>
            <ListItem margin="15px 0 0" textAlign="justify">Decir que intentaremos hacer las cosas en un menor plazo al pronosticado significa que no estamos haciendo todo lo posible en este momento</ListItem>
            <ListItem margin="15px 0 0" textAlign="justify">Si no tienes un plan nuevo o un aproximación diferente que en efecto permita terminar antes, simplemente está mintiendo</ListItem>
            <ListItem margin="15px 0 0" textAlign="justify">El costo de decir que sí cuando no es posible, simplemente es un desastre inminente.  </ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="primary" textColor="tertiary">
          <Heading size={6} textColor="quaternary" caps>
          Bibliografia
          </Heading>
          <List>
            <ListItem margin="15px 0 0" textAlign="justify">The Clean Coder A Code of Conduct for Professional Programmers, paginas 23 al 43</ListItem>
            <ListItem margin="15px 0 0" textAlign="justify">https://psicologia-estrategica.com/aprender-decir-no-poner-limites/  </ListItem>
            <ListItem margin="15px 0 0" textAlign="justify">https://www.iconfinder.com/icons/3099346/2_gesturing_icon  </ListItem>
            <ListItem margin="15px 0 0" textAlign="justify">https://i.ytimg.com/vi/wIFLnEehNTE/maxresdefault.jpg  </ListItem>
          </List>
        </Slide>
      </Deck>
    );
  }
}
