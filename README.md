

1. Install dependencies

   ```sh
   yarn install
   ```

2. Start the webpack server. The server will run at [`localhost:3000`](http://localhost:3000).

   ```sh
   yarn start
   ```


## Build & Deployment

Building the dist version of the project is as easy as running

```sh
yarn build
```

If you want to deploy the slideshow to surge, run

```sh
yarn deploy
```
